# JuegoNim

Juego de estrategia (dos jugadores) en el que se implementa el algoritmo minimax con poda.

## Materiales
Para jugar Nim se necesita tener 10 tarjetas.

##  Reglas
Las acciones permitadas son tomar 1,2 o 3 tarjetas por turno.

## Objetivo
Que el que tome la ultima carta pierde.

## Implementación

El algoritmo se implemento en el lenguaje de programación Groovy