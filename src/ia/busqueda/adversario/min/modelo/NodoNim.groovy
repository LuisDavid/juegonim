package ia.busqueda.adversario.min.modelo

class NodoNim extends Nodo{
	int estado //Indica en que estado se encuentra el nodo -1 es un estado perdedor 1 es un ganador
	Turno turno
	int estadoJuego //Indica el numero de piezas que hay en la mesa
	
	@Override
	String toString() {
		return "estado: "+estado+" turno: "+turno+" estadoJuego: "+estadoJuego+" valor: "+valor
	}
}
