package ia.busqueda.adversario.min

import groovy.util.logging.Log
import ia.busqueda.adversario.min.modelo.Movimiento
import ia.busqueda.adversario.min.modelo.Nodo
import ia.busqueda.adversario.min.modelo.NodoNim
import ia.busqueda.adversario.min.modelo.Turno
@Log
class JuegoNim {
	int numCartas;
	Turno turno
	JuegoNim(int numCartas, Turno t) {
		this.numCartas=numCartas
		this.turno=t;
	}
	void tomarCarta(Movimiento m){
		numCartas-=m.valor
	}
	NodoNim dameSucesor(NodoNim nodo, Movimiento movimiento) {
		int cartasActuales=nodo.estadoJuego-movimiento.valor
		if( cartasActuales >= 0) {
			return new NodoNim(valor:movimiento,estado:0,estadoJuego: cartasActuales)
		}
		null
	}
	List dameSucesores(NodoNim nodo){
		NodoNim sucesor
		List sucesores=[]
		Turno turnoPosterior=dameSiguienteTurno(nodo.turno)
		for(Movimiento m: Movimiento.values()) {
			sucesor=dameSucesor(nodo,m)
			if(sucesor) {
				sucesor.turno=turnoPosterior
				sucesores.add(sucesor)
			}
		}
		return sucesores
	}
	int funcionEvaluacion(NodoNim nodo) {
		if(nodo.turno==Turno.JUGADOR) {
			return -1
		}
		return 1
	}
	boolean esEstadoFinal(NodoNim nodo) {
		if(nodo.estadoJuego==0) {
			return true
		}
		return false
	}
	private Turno dameSiguienteTurno(Turno t) {
		if(t==Turno.PC) {
			return Turno.JUGADOR
		}
		return Turno.PC
	}
	Movimiento getMovientoNumCartas(int n) {
		for(Movimiento m : Movimiento.iterator()) {
			if(m.valor==n && numCartas-n>=0) {
				return m
			}
		}
		return null
	}
}
