package ia.busqueda.adversario.min

import groovy.util.logging.Log
import ia.busqueda.adversario.min.modelo.Movimiento
import ia.busqueda.adversario.min.modelo.NodoNim
import ia.busqueda.adversario.min.modelo.Turno
@Log
class Principal {
	static void main(String []args) {
		JuegoNim nim=new JuegoNim(10, Turno.PC)
		MiniMax pc=new MiniMax(nim,10)
		int m;
		Scanner scan = new Scanner(System.in)
		NodoNim nodoNim
		Random r= new Random()
		Turno t=r.next(2)==1?Turno.PC:Turno.JUGADOR
		Movimiento movimiento;
		while(nim.numCartas>0) {
			
			if(t==Turno.JUGADOR) {
				println "---------------------------------------------------------"
				println "- Turno: Jugador										 -"
				println "- Numero de cartas disponibles: "+nim.numCartas+"		 -"
				println "---------------------------------------------------------"
				
				while(true){
					println "¿Cuantas cartas vas a tomar?"
					m=Integer.parseInt(scan.nextLine())
					movimiento=nim.getMovientoNumCartas(m)
					if(movimiento) {
						nim.tomarCarta(movimiento)
						break;
					}
					else {
						println "No puedes tomar ${m} intentalo de nuevo"
					}
				};
				t=nim.dameSiguienteTurno(t)
			}
			if(t==Turno.PC && nim.numCartas!=0) {
				println "---------------------------------------------------------"
				println "- Turno: PC											 -"
				println "- Numero de cartas disponibles: "+nim.numCartas+"		 -"
				println "---------------------------------------------------------"
				
				nodoNim =pc.desicionMinimaxAB(new NodoNim(valor:1,turno:Turno.PC,estado:0,estadoJuego: nim.numCartas),10)
				println "PC numPodados: "+pc.numPodados
				if(nodoNim) {
					println "PC toma: "+nodoNim?.valor
					nim.tomarCarta(nodoNim.valor)
				}
				else {
					println "Como se que eres inteligente te dire que ya tienes el juego ganado."
					t=Turno.JUGADOR
					break; 
				}
				t=nim.dameSiguienteTurno(t)
			}
		}
		println t==Turno.JUGADOR?"¡¡¡¡¡¡¡¡¡Felicidades Ganaste!":"¡¡¡¡¡¡¡¡¡Perdiste!"
		
		
	}
	
	
}
