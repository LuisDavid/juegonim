package ia.busqueda.adversario.min

import groovy.util.logging.Log
import ia.busqueda.adversario.min.modelo.Nodo
import ia.busqueda.adversario.min.modelo.NodoNim
import ia.busqueda.adversario.min.modelo.Turno

@Log
class MiniMax {
	final int MINIMO_VALOR=-1
	final int MAXIMO_VALOR=1
	int numPodados;
	JuegoNim nim;
	
	
	MiniMax(JuegoNim nim, int p){
		this.nim= nim
		this.numPodados=0;
	}
	
	NodoNim desicionMinimaxAB(NodoNim actual,int profundidad){
		this.numPodados=0;
		int valorActual
		NodoNim maxNodo
		int alfa
		alfa=MINIMO_VALOR
		List sucesores = nim.dameSucesores(actual)
		NodoNim nodo
		for(int i=0; i<sucesores.size; i++) {
			nodo=sucesores.get(i)
			valorActual=valorMinimaxAB(nodo, profundidad-1, alfa, MAXIMO_VALOR)
			if(valorActual>alfa) {
				alfa=valorActual
				maxNodo=nodo
			}
			if(alfa>=MAXIMO_VALOR) {
				return maxNodo
			}
			
		}
		return maxNodo
	}
	int valorMinimaxAB(NodoNim nodo,int profundidad,int alfa, int beta) {
		List sucesores = nim.dameSucesores(nodo)
		if(nim.esEstadoFinal(nodo) || profundidad==0 || sucesores == []) {
			return nim.funcionEvaluacion(nodo)
		}
		else if(nodo.turno==Turno.PC){
			return maximizadorAB(sucesores, profundidad-1, alfa, beta)
		}
		else{
			return minimizadorAB(sucesores, profundidad-1, alfa, beta)
		}
		return 0
	}
	
	int maximizadorAB(List sucesores, int profundidad,int alfa, int beta) {
		int valorActual=0;
		for(NodoNim nodo: sucesores) {
			valorActual=valorMinimaxAB(nodo, profundidad, alfa, beta)
			if(valorActual>alfa) {
				alfa=valorActual
			}
			if(alfa>=beta) {
				numPodados++;
				return valorActual //Poda alfa
			}
		}
		return alfa
	}
	int minimizadorAB(List sucesores, int profundidad,int alfa, int beta) {
		int valorActual=0;
		for(NodoNim nodo: sucesores) {
			valorActual=valorMinimaxAB(nodo, profundidad, alfa, beta)
			if(valorActual<beta) {
				beta=valorActual
			}
			if(alfa>=beta) {
				numPodados++;
				return valorActual //Poda beta
			}
		}
		return beta
	}
}
